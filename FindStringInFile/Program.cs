﻿using System;
using System.IO;

namespace FindStringInFile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;

            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Enter a text to find in the txt file");
            string input = Console.ReadLine();

            string path = "C:/users/tjohn/documents/test.txt";
            string content = File.ReadAllText(path);
            //Console.WriteLine(content);

            if (input != "")
            {
                int matchIndex = -1;
                for (int i = 0; i < content.Length; i++)
                {
                    Boolean match = true;
                    for(int j = 0; j < input.Length; j++)
                    {
                        if(input[j] != content[i + j])
                        {
                            match = false;
                            break;
                        }
                    }
                    if (match)
                    {
                        matchIndex = i;
                        break;
                    }
                }
                if(matchIndex == -1)
                {
                    Console.WriteLine("The given input string is not found in the txt file");
                }
                else
                {
                    Console.WriteLine("A match of the input string is found at index {0} of the file text", matchIndex);
                }
            }
            else
            {
                Console.Beep(1100, 200);
                Console.WriteLine("You did not input any value");
            }

        }
    }
}
